output "nonprod_new_security_group_id_ssh" {
  description = "This is the AWS Security Group ID SSH"
  value       = module.nonprod_new_aws_security_group.security_group_id_ssh
}

output "nonprod_new_security_group_id_web" {
  description = "This is the AWS Security Group ID WEB"
  value       = module.nonprod_new_aws_security_group.security_group_id_web
}

output "nonprod_new_vpc_id" {
  description = "This is the new VPC ID"
  value       = module.nonprod_new_aws_vpc.nonprod_new_vpc_id
}

output "nonprod_new_vpc_subnet_id" {
  description = "This is the new VPC SUBNET ID"
  value       = module.nonprod_new_aws_vpc.nonprod_new_vpc_subnet_id
}
