
//--------------------------------------------------------------------
// Modules

module "nonprod_new_aws_vpc" {
  source  = "app.terraform.io/beaulabs/aws-vpc/aws"
  version = "1.1.0"

  name   = "beau25mar"
  region = "us-west-1"
}


module "nonprod_new_aws_security_group" {
  source  = "app.terraform.io/beaulabs/aws-security-group/aws"
  version = "1.0.9"

  name       = "beau25mar"
  new_vpc_id = module.nonprod_new_aws_vpc.nonprod_new_vpc_id
}

